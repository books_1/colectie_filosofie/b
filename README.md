# B

## Content

```
./B. G. Kuznetov:
B. G. Kuznetov - Ratiune si fiintare.pdf

./Barry Commoner:
Barry Commoner - Cercul care se inchide.pdf

./Basile Tatakis:
Basile Tatakis - Filosofia bizantina.pdf

./Benedict Spinoza:
Benedict Spinoza - Etica.pdf

./Ben Goldacre:
Ben Goldacre - Pseudostiinta un medic descopera coruptia din spatele industriei de medicamente.pdf

./Benjamin Constant:
Benjamin Constant - Scrieri politice.pdf

./Bernard Williams:
Bernard Williams - Moralitatea.pdf

./Bertrand Russell:
Bertrand Russell - Cunoasterea lumii exterioare.pdf
Bertrand Russell - De ce nu sunt crestin.pdf
Bertrand Russell - In cautarea fericirii.pdf
Bertrand Russell - Istoria filosofiei occidentale I.pdf
Bertrand Russell - Istoria filosofiei occidentale II.pdf
Bertrand Russell - Misticism si logica.pdf
Bertrand Russell - Problemele filosofiei (vechi).pdf
Bertrand Russell - Problemele filosofiei.pdf
Bertrand Russell - Religie si stiinta.pdf

./Blaise Pascal:
Blaise Pascal - Cugetari.pdf
Blaise Pascal - Scrieri alese.pdf

./Boetius din Dacia:
Boetius din Dacia - Despre viata filosofului.pdf
Boetius din Dacia - Mangaierile filosofiei.pdf
Boetius din Dacia - Tratate teologice.pdf

./Bogdan Popoveniuc:
Bogdan Popoveniuc - Iluziile ratiunii.pdf

./Brian Davies:
Brian Davies - Introducere in filozofia religiei.pdf

./Brian Greene:
Brian Greene - Universul elegant.pdf

./Brian May:
Brian May - Bang.pdf

./Brice Parain:
Brice Parain - Logosul platonician.pdf
```

